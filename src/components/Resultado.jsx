import React from "react";
import styled from "@emotion/styled";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import PropTypes from 'prop-types';

const Mensaje = styled.p`
    background-color: rgb(127, 224, 237);
    margin-top: 2rem;
    padding: 1rem;
    text-align: center;
    color: grey;
`;

const TextoCotizacion = styled.p`
    color: #00838f;
    padding: 1rem;
    text-transform: uppercase;
    font-weight: bold;
    margin: 0;
`;

const Resultado = ({cotizacion}) => {

    return(
        (cotizacion === 0) ? <Mensaje> Elige marca, anio y tplan </Mensaje> :
            <TransitionGroup
                component="p"
                className="resultado"
            >
                <CSSTransition
                    classNames="resultado"
                    key={ cotizacion }
                    timeout={{ enter: 500, exit: 500 }}
                >
                    <TextoCotizacion> El total es: { cotizacion } </TextoCotizacion>
                </CSSTransition>
            </TransitionGroup>
    );

};

Resultado.propTypes = {
    cotizacion : PropTypes.number.isRequired
}

export default Resultado;